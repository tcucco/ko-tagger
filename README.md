Knockout Tagger
===============

An Autocomplete Tagging Control Utilizing Knockout
--------------------------------------------------

There are lots of decent autocomplete and tagging controls out there, but I've personally had a
tough time getting them to work properly with Knockout, my preferred data binding library. Not only
that, but they have many options and customizations, which are great, but which I never need. And,
without Knockout or some other data binding library as a dependency, they have to write a lot more
code to get the same effect you can get if you can rely on Knockout.

With these realizations, I've put together the Knockout Tagger, it consists of 4 parts:

  1. A custom binding handler to use with contenteditable elements
  2. A view model that holds the list of options, selections, and handles filtering and selection
  3. An HTML template to display the tagger
  4. Some CSS to style the tagger

The first two items are found in tagger.js, the HTML template resides in index.html, and the CSS is
in, you guessed it, tagger.css. For an example of how to use the tagger, check out index.html.

This is not meant to be a full featured tagger, but rather one with just the functionality I
typically use. Also, I've not tried to set this up to be easily distributable (obviously), it's more
a proof of concept for myself, although you may find it useful too as a starting point. To use it in
a project you'll need to take the code out of tagger.js, tagger.css and the template from index.html
and paste them into your project in the "appropriate places".
