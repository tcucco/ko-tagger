(function(ns) {
  "use strict";

  // DEPENDENCIES: ko, underscore, jquery
  //
  // underscore and jquery could easily be removed, but I'm developing this for projects that
  // already have those so I'll go ahead and use them.

  ko.bindingHandlers.koTaggerContentEditable = {
    init: function(element, valueAccessor) {
      function keydown(e) {
        if (e.which == 13) {
          return false;
        }
      }
      function keyup(e) {
        valueAccessor()($(element).text());
      }

      $(element).on("keydown", keydown);
      $(element).on("keyup", keyup);

      ko.utils.domNodeDisposal.addDisposeCallback(element, function() {
        $(element).off("keydown", keydown);
        $(element).off("keyup", keyup);
      });
    },
    update: function(element, valueAccessor) {
      var v = ko.unwrap(valueAccessor());
      var $e = $(element);
      if ($e.text() != v) {
        $e.text(v);
      }
    }
  };

  ko.bindingHandlers.koTaggerVisible = {
    init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
      if (ko.bindingHandlers.visible.init) {
        ko.bindingHandlers.visible.init(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);
      }
    },
    update: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
      var vis = ko.unwrap(valueAccessor());

      if (ko.bindingHandlers.visible.update) {
        ko.bindingHandlers.visible.update(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);
      }

      var classRe = /\b\s*ko-tagger-top\s*\b/;

      if (vis) {
        setTimeout(function() {
          var offset = $(element).offset();
          var showTop = offset.top > window.innerHeight / 2;
          var hasClass = classRe.test(element.className);

          if (showTop && !hasClass) {
            element.className += " ko-tagger-top";
          } else if (!showTop && hasClass) {
            element.className = element.className.replace(classRe, "");
          }
        });
      } else {
        element.className = element.className.replace(classRe, "");
      }
    }
  };

  function TaggingViewModel(args) {
    // args:
    //   options (required)
    //     an array or observable array of objects. If options is an observable array, it is
    //     directly assigned to the view models options property. Otherwise an observable array is
    //     constructed from options.
    //   label
    //     the property to use as a label, or a function to compute the label. If not given, the
    //     entire option is returned. This is good when the options are an array of strings.
    //   selectedOptions
    //     items that start out selected
    //   createOption
    //     a function to create a new option from entered text, if the user enters text that does
    //     not match the label of any existing options. If not present new options cannot be
    //     created.
    //   delimiters
    //     a string of characters to use as tag delimiters. When one of these is entered the current
    //     entry will be used to add an option and the search text will be cleared. Defaults to ","
    //   initialSearchText
    //     string with which to initialize searchText
    //   alwaysShow
    //     if true, the list will always show when when the control has focus
    //   maxSelectedItems
    //     maximum number of selected items
    //   sortOptions
    //     sort options by label. Defaults to true
    //   match
    //     a function that compares each option to the entered text to determine if they match. By
    //     default the lowered search text is compared to the lowered labels. The return value
    //     should be an array where the first value is boolean determining if there is a match, and
    //     the second value should be a number indicating match strength, where a lower number
    //     means a stronger match.

    args = _.extend({}, defaultArgs, args);

    var self = this;
    var isOpen = false;
    var closeOnClick = true;
    var keyHandled = false;
    var forceClose = false;
    var selectedOptionsType = null;

    function setupOptions() {
      // The selectedOptions array will be setup differently depending on args.
      // 1. If args.selectedOptions is a ko.observableArray, then it is set directly to
      //    selectedOptions. This way changes to the selection are transparently passed to the
      //    original array.
      // 2. If args.selectedOptions is a single ko.observable, then this is considered to be a
      //    'single selection' list. The selectedOptions array will be constructed with the value
      //    of args.selectedOptions as its only element, and _singleOption will be assigned for
      //    args.selectedOptions. Calls to assignOption will both edit _singleOption and update
      //    selectedOptions so that things appear to work as with the multi select mode.
      // 3. If args.selectedOptions is anything else, it's considered to be an array and is just
      //    passed to initialize a new ko.observableArray.

      if (isObservableArray(args.selectedOptions)) {
        self.selectedOptions = args.selectedOptions;
        selectedOptionsType = "multi";
      } else if (isObservable(args.selectedOptions)) {
        self._singleOption = args.selectedOptions;
        self.selectedOptions = ko.observableArray([]);
        self._singleOption.subscribe(setOptionsFromSingle);
        setOptionsFromSingle();
        selectedOptionsType = "single";
      } else {
        self.selectedOptions = ko.observableArray(args.selectedOptions);
        selectedOptionsType = "multi";
      }
    }

    function setOptionsFromSingle() {
      var o = self._singleOption();
      if (o === null || o === undefined) {
        self.selectedOptions([]);
      } else {
        self.selectedOptions([o]);
      }
    }

    function optionToLabel(opt) {
      // Convertions an option to the displayed label using the args.label property.

      if (args.label) {
        if (_.isFunction(args.label)) {
          return args.label(opt);
        } else {
          return opt[args.label];
        }
      } else {
        return opt;
      }
    }

    function optionToFilteredOption(opt, strength) {
      // Converts an option into a filtered option object which includes the value, data and match
      // strength.

      return { data: opt, label: optionToLabel(opt), strength: strength };
    }

    function compareFilteredOptions(l, r) {
      // Compares two options based on their match strength. If their strength is equal, it compares
      // them based on label.

      if (l.strength == r.strength) {
        if (l.label < r.label) {
          return -1;
        } else if (l.label > r.label) {
          return 1;
        } else {
          return 0;
        }
      } else {
        return l.strength - r.strength;
      }
    }

    function defaultMatch(searchText, option) {
      // The default matching function, to be used in the case that args.match is not supplied.
      // Returns a tuple of [matches, strength] where matches is boolean and strength is the
      // strength of the match, lower is stronger.

      var idx = optionToLabel(option).toLowerCase().indexOf(searchText.toLowerCase());
      return [idx >= 0, idx];
    }

    function getFilteredOptions() {
      // Returns a list of all options that should show up as matches in the filtered list.

      var filteredOptions = [];
      var searchText = self.searchText() || "";
      var matchFx = self.match || defaultMatch;

      for (var oidx = 0; oidx < self.options().length; ++oidx) {
        var opt = self.options()[oidx];

        if (self.selectedOptions.indexOf(opt) != -1) {
          continue;
        }

        if (searchText) {
          var match = matchFx(searchText, opt);

          if (match[0]) {
            filteredOptions.push(optionToFilteredOption(opt, match[1]));
          }
        } else {
          if (args.alwaysShow) {
            filteredOptions.push(optionToFilteredOption(opt, 0));
          }
        }
      }

      if (args.sortOptions) {
        filteredOptions.sort(compareFilteredOptions);
      }

      var rawFilteredOptions = _.map(filteredOptions, function(o) { return o.data; });

      if (self.selectedOption() && _.indexOf(rawFilteredOptions, self.selectedOption()) == -1) {
        self.selectedOption(null);
      }

      return rawFilteredOptions;
    }

    function getShowOptions() {
      // Function for showOptions ko.computed.

      var newValue = self.isEnabled() && (self.hasFocus() || self.forceOpen()) && self.filteredOptions().length != 0;

      if (newValue != isOpen) {
        if (newValue) {
          $("body").on("click", onBodyClick);
        } else {
          $("body").off("click", onBodyClick);
        }

        isOpen = newValue;
      }

      return newValue;
    }

    function close() {
      self.hasFocus(false);
      self.forceOpen(false);
    }

    function clear(all) {
      self.searchText("");
      self.selectedOption(null);
      if (all) {
        close();
      }
    }

    function isFull() {
      return args.maxSelectedItems && self.selectedOptions().length <= args.maxSelectedItems || selectedOptionsType == "single";
    }

    function _addOption(opt) {
      assignOption(opt);

      self.searchText("");

      if (isFull()) {
        forceClose = true;
        close();
      }
    }

    function assignOption(opt, atIndex, replace) {
      if (selectedOptionsType == "multi") {
        if (atIndex === undefined || atIndex === null || atIndex < 0) {
          if (isFull()) {
            atIndex = self.selectedOptions().length - 1;
            replace = true;
          } else {
            atIndex = self.selectedOptions().length;
          }
        }
        self.selectedOptions.splice(atIndex, replace ? 1 : 0, opt);
      } else {
        self._singleOption(opt);
        setOptionsFromSingle();
      }
    }

    function addOption() {
      if (self.selectedOption()) {
        _addOption(self.selectedOption());
      } else if (self.searchText() && args.createOption) {
        var newOption = args.createOption(self.searchText());
        // TODO: Should we add newOption to self.options here, or should we leave that up to
        //       args.createOption? For now I'm going to leave it up to createOption, so that the
        //       author has an option as to whether or not they want to change the list.
        _addOption(newOption);
      } else if (self.filteredOptions().length != 0) {
        _addOption(self.filteredOptions()[0]);
      } else {
        return;
      }
      clear(false);
    }

    function _replaceOption(opt, at) {
      if (at === undefined) {
        at = self.selectedOptions().length - 1;
      }
      self.selectedOptions.splice(at, 1, opt);
    }

    function removeOption(opt) {
      if (selectedOptionsType == "multi") {
        if (opt === undefined) {
          self.selectedOptions.pop();
        } else {
          self.selectedOptions.remove(opt);
        }
      } else {
        self._singleOption(null);
        setOptionsFromSingle();
      }
    }

    function selectAdjacent(dir) {
      if (self.filteredOptions().length == 0) { return; }

      if (!self.selectedOption()) {
        self.selectedOption(self.filteredOptions()[0]);
      } else {
        var idx = _.indexOf(self.filteredOptions(), self.selectedOption());
        var newIdx = null;

        if (idx == -1) {
          newIdx = 0;
        } else {
          newIdx = idx + dir;
          if (newIdx < 0) {
            newIdx = 0;
          } else if (newIdx >= self.filteredOptions().length) {
            newIdx = self.filteredOptions().length - 1;
          }
        }

        self.selectedOption(self.filteredOptions()[newIdx]);
      }
    }

    function selectNext() {
      selectAdjacent(1);
    }

    function selectPrevious() {
      selectAdjacent(-1);
    }

    function onBodyClick(e) {
      if (closeOnClick) {
        close();
      }
      closeOnClick = true;
    }

    function onClick(vm, e) {
      closeOnClick = false;
      if (forceClose) {
        forceClose = false;
      } else {
        self.hasFocus(true);
      }
    }

    function onOptionClick(m, e) {
      closeOnClick = false;
      _addOption(m)
    }

    function onRemoveOptionClick(m, e) { removeOption(m); }

    function scrollTo() {
      var element = $(".ko-tagger-option.selected");
      var container = element.parent();
      scrollIntoView(element[0], container[0]);
    }

    function onSearchTextKeyDown(vm, e) {
      switch (e.which) {
      case 8: // Backspace
        if (!self.searchText()) {
          removeOption();
        }
        keyHandled = true;
        break;
      case 9: // Tab
        self.forceOpen(false);
        keyHandled = true;
        break;
      case 13: // Enter
        addOption();
        keyHandled = true;
        return false;
      case 40: // Down
        selectNext();
        keyHandled = true;
        scrollTo();
        return false;
      case 38: // Up
        selectPrevious();
        keyHandled = true;
        scrollTo();
        return false;
      }
      return true;
    }

    function onSearchTextKeyPress(vm, e) {
      if (!keyHandled) {
        var char = String.fromCharCode(e.charCode);

        for (var i = 0; i < args.delimiters.length; ++i) {
          if (char == args.delimiters.charAt(i)) {
            keyHandled = true;
            addOption();
            return false;
          }
        }
      }
      return true;
    }

    function onSearchTextKeyUp(vm, e) {
      if (!keyHandled) {
        switch (e.which) {
        case 27:
          clear(true);
          break;
        }
      }
      keyHandled = false;
      return true;
    }

    self.searchText = ko.observable(args.initialSearchText);
    self._hasFocus = ko.observable(false);
    self.hasFocus = ko.computed({
      owner: self,
      read: self._hasFocus,
      write: function(nv) {
        if (self.isEnabled()) {
          if (nv) {
            self.forceOpen(true);
          }
          self._hasFocus(nv);
        } else {
          self._hasFocus(false);
        }
      }
    });
    self.forceOpen = ko.observable(false);
    self.options = isObservableArray(args.options) ? args.options : ko.observableArray(args.options);
    self.selectedOption = ko.observable(null);
    self.selectedOptions = null;
    self._singleOption = null;

    setupOptions();

    self.filteredOptions = ko.computed(getFilteredOptions);
    if ("isEnabled" in args) {
      if (_.isFunction(args.isEnabled)) {
        self.isEnabled = ko.computed(args.isEnabled);
      } else {
        self.isEnabled = ko.observable(Boolean(args.isEnabled));
      }
    } else {
      self.isEnabled = ko.observable(true);
    }
    self.showOptions = ko.computed(getShowOptions);
    self.match = args.match;

    self.optionToLabel = optionToLabel;
    self.click = onClick;
    self.optionClick = onOptionClick;
    self.removeOptionClick = removeOption;
    self.keyDown = onSearchTextKeyDown;
    self.keyPress = onSearchTextKeyPress;
    self.keyUp = onSearchTextKeyUp;
  }

  function scrollIntoView(element, container) {
    var containerTop = $(container).scrollTop();
    var containerBottom = containerTop + $(container).height();
    var elemTop = element.offsetTop;
    var elemBottom = elemTop + $(element).height();
    if (elemTop < containerTop) {
      $(container).scrollTop(elemTop);
    } else if (elemBottom > containerBottom) {
      $(container).scrollTop(elemBottom - $(container).height());
    }
}

  function isObservable(a) {
    return ko.isObservable(a) && !("push" in a);
  }

  function isObservableArray(a) {
    return ko.isObservable(a) && "push" in a;
  }

  var defaultArgs = {
    options: [],
    label: null,
    selectedOptions: [],
    createOption: null,
    delimiters: ",",
    initialSearchText: "",
    alwaysShow: false,
    maxSelectedItems: null,
    sortOptions: true,
    match: null
  };

  ns.TaggingViewModel = TaggingViewModel;
})(window);
